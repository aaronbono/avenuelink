package com.avenuelink.interview.assignment.bowling.beans;

public class Frame {
	
	private int firstAttempt;
	
	private Integer secondAttempt = null;
	
	private Frame followingFrame = null;
	
	/**
	 * A Frame is made only after the first attempt so store that value immediately.
	 */
	public Frame(int firstAttempt) {
		this.firstAttempt = firstAttempt;
	}
	
	public int getFirstAttempt() {
		return firstAttempt;
	}

	public Integer getSecondAttempt() {
		return secondAttempt;
	}

	public void setSecondAttempt(Integer secondAttempt) {
		this.secondAttempt = secondAttempt;
	}
	
	public void setFollowingFrame(Frame followingFrame) {
		this.followingFrame = followingFrame;
	}
	
	public boolean isStrike() {
		return firstAttempt == 10 && (secondAttempt == null || secondAttempt == 0);
	}
	
	public boolean isSpare() {
		return
			(firstAttempt < 10)
			&&
			(secondAttempt != null)
			&&
			(firstAttempt + secondAttempt == 10)
		;
	}
	
	/**
	 * The score of the current frame is:
	 * <ul>
	 *   <li>Strike: 10 + next two tries in following frames</li>
	 *   <li>Spare: 10 + next one try in following frame</li>
	 *   <li>Else: sum of the two attempts in this frame</li>
	 * </ul>
	 * Treat nulls as 0.
	 */
	public int getCurrentScore() {
		if (isStrike()) {
			return 10 + ((followingFrame == null) ? 0 : followingFrame.getTwoAttempts());
		} else if (isSpare()) {
			return 10 + ((followingFrame == null) ? 0 : followingFrame.getOneAttempt());
		} else {
			return firstAttempt + ((secondAttempt == null) ? 0 : secondAttempt);
		}
	}
	
	int getOneAttempt() {
		return firstAttempt;
	}
	
	int getTwoAttempts() {
		if (isStrike()) {
			// This is a strike so get this frame's score plus the first try of the next frame.
			return 10 + ((followingFrame == null) ? 0 : followingFrame.getOneAttempt());
		} else {
			// Not a strike
			return firstAttempt + ((secondAttempt == null) ? 0 : secondAttempt);
		}
	}
	
	public boolean isValid() {
		return
			isStrike()
			||
			(secondAttempt == null && firstAttempt >= 0 && firstAttempt < 10)
			||
			(
				secondAttempt != null
				&&
				firstAttempt >= 0
				&&
				secondAttempt >= 0
				&&
				(firstAttempt + secondAttempt) <= 10
			)
		;
	}
	
	public boolean isComplete() {
		return isStrike() || (secondAttempt != null);
	}
	
	public String toString() {
		if (isStrike()) {
			return "X ";
		} else if (isSpare()) {
			return (firstAttempt == 0 ? "-" : Integer.toString(firstAttempt)) + "/";
		} else if (secondAttempt == null) {
			return (firstAttempt == 0 ? "-" : Integer.toString(firstAttempt)) + "_";
		} else {
			return (firstAttempt == 0 ? "-" : Integer.toString(firstAttempt))
					+ (secondAttempt == 0 ? "-" : secondAttempt.toString());
		}
	}

}



package com.avenuelink.interview.assignment.bowling.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Game {
	
	private List<Frame> frames = new ArrayList<Frame>();
	
	public List<Frame> getFrames() {
		return Collections.unmodifiableList(frames);
	}
	
	public void addFrame(Frame frame) {
		if (frames.size() > 0) {
			frames.get(frames.size() - 1).setFollowingFrame(frame);
		}
		this.frames.add(frame);
	}
	
	/**
	 * Get the score of the current game.
	 */
	public int getCurrentScore() {
		int score = 0;
		for (int index = 0; index < frames.size() && index < 10; ++index) {
			Frame frame = frames.get(index);
			score += frame.getCurrentScore();
		}
		return score;
	}
	
	/**
	 * Determine if the game is valid so far.  This will return true if it is a
	 * game in progress and each frame is valid up to this point.
	 */
	public boolean isValid() {
		for (Frame frame : frames) {
			if (! frame.isValid()) {
				return false;
			}
		}
		if (frames.size() == 12) {
			// The last 3 frames must be a strike.
			return frames.get(9).isStrike() && frames.get(10).isStrike() && frames.get(11).isStrike();
		} else if (frames.size() == 11) {
			// The 10th frame must be a strike or spare.  If a spare, they only get
			// the first attempt on the 11th frame.
			if (frames.get(9).isSpare()) {
				return frames.get(10).getSecondAttempt() == null;
			} else {
				return true;
			}
		} else {
			return frames.size() <= 10;
		}
	}
	
	/**
	 * For the game to be complete / over / done, all 10 frames must be played and
	 * if the last frame is a spare or strike, one (spare) or two (strike) more
	 * attempts must be made.
	 */
	public boolean isComplete() {
		if (! isValid()) {
			return false;
		} else if (frames.size() < 10) {
			return false;
		} else {
			Frame tenthFrame = frames.get(9);
			if (tenthFrame.isStrike()) {
				if (frames.size() > 10) {
					Frame eleventhFrame = frames.get(10);
					return
						(eleventhFrame.isStrike() && frames.size() == 12)
						||
						(eleventhFrame.getSecondAttempt() != null)
					;
				} else {
					return false;
				}
			} else if (tenthFrame.isSpare()) {
				return (frames.size() == 11);
			} else {
				return tenthFrame.getSecondAttempt() != null;
			}
		}
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		int index = 0;
		for (Frame frame : frames) {
			++index;
			if (index > 10) {
				if (index == 11) {
					builder.append("|");
				}
				builder.append(frame.toString());
			} else {
				builder.append(frame.toString()).append("|");
			}
		}
		while (index < 10) {
			builder.append("__|");
			++index;
		}
		return builder.toString().trim();
	}

}

package com.avenuelink.interview.assignment.bowling;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.avenuelink.interview.assignment.bowling.beans.Frame;
import com.avenuelink.interview.assignment.bowling.beans.Game;

public class BowlingApp {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BowlingApp.class);
	
	private List<Game> games;
	
	public BowlingApp(String score) throws ParseException {
		this.games = new ArrayList<Game>(1);
		this.games.add(parseScore(score));
	}

	public BowlingApp(List<String> gameStrings) throws IOException, ParseException {
		this.games = new ArrayList<Game>(gameStrings.size());
		addGames(gameStrings);
	}
	
	List<Game> getGames() {
		return games;
	}

	private void addGames(List<String> gameStrings) throws ParseException {
		StringBuilder errors = new StringBuilder();
		int firstErrorLine = -1;
		int lineNumber = -1;
		for (String gameString : gameStrings) {
			++lineNumber;
			LOGGER.debug("Reading line " + lineNumber + " " + gameString);
			try {
				if (StringUtils.isNotBlank(gameString)) {
					games.add(parseScore(gameString));
				}
			} catch (ParseException e) {
				if (firstErrorLine < 0) {
					firstErrorLine = lineNumber;
				}
				errors
					.append("Error parsing ")
					.append(gameString)
					.append(": ")
					.append(e.getMessage())
					.append("\n")
				;
			}
		}
		
		if (errors.length() > 0) {
			throw new ParseException(errors.toString().trim(), lineNumber);
		}
	}
	
	private Game parseScore(String score) throws ParseException {
		LOGGER.debug("Parsing score " + score);
		
		score = score.replaceAll("-", "0");
		String[] frames = score.split("\\|");
		Game game = new Game();
		for (int index = 0; index < frames.length; ++index) {
			String frameString = frames[index];
			LOGGER.debug("Processing frame " + (index + 1) + " " + frameString);
			
			if (frameString.length() > 2) {
				throw new ParseException(
					"Game " + score + " has invalid " + (index + 1) + " frame " + frameString,
					index
				);
			}
			
			String firstTry = (frameString.length() > 0) ? frameString.substring(0, 1) : null;
			String secondTry = (frameString.length() > 1) ? frameString.substring(1) : null;
			if (index < 10) {
				// Frame 1 - 10
				if ("x".equalsIgnoreCase(firstTry) && secondTry == null) {
					LOGGER.debug("Strike!");
					Frame frame = new Frame(10);
					game.addFrame(frame);
				} else if (
					(firstTry != null && StringUtils.isNumeric(firstTry))
					&&
					(secondTry != null && StringUtils.isNumeric(secondTry))
				) {
					Frame frame = new Frame(Integer.parseInt(frameString.substring(0, 1)));
					frame.setSecondAttempt(Integer.parseInt(frameString.substring(1)));
					game.addFrame(frame);
					LOGGER.debug("Bowled " + frame);
				} else if (
					(firstTry != null && StringUtils.isNumeric(firstTry))
					&&
					(secondTry != null && (StringUtils.isNumeric(secondTry) || "/".equals(secondTry)))
				) {
					Frame frame = new Frame(Integer.parseInt(frameString.substring(0, 1)));
					frame.setSecondAttempt(10 - frame.getFirstAttempt());
					game.addFrame(frame);
					LOGGER.debug("Bowled " + frame);
				} else {
					throw new ParseException(
						"Game " + score + " has invalid " + (index + 1) + " frame " + frameString,
						index
					);
				}
			} else if (index == 10) {
				if (frameString.length() > 0) {
					throw new ParseException(
						"Game " + score + " has invalid bonus ball delimiter",
						score.lastIndexOf('|')
					);
				}
			} else if (index == 11) {
				if (frameString.length() > 2) {
					throw new ParseException(
						"Game " + score + " has invalid bonus balls " + frameString,
						score.lastIndexOf("|")
					);
				} else if (frameString.length() > 0) {
					char firstChar = frameString.charAt(0);
					if (firstChar == 'x' || firstChar == 'X') {
						LOGGER.debug("Bonus Strike!");
						game.addFrame(new Frame(10));
						if (frameString.length() > 1) {
							char secondChar = frameString.charAt(1);
							if (secondChar == 'x' || secondChar == 'X') {
								LOGGER.debug("Bonus Second Strike!");
								game.addFrame(new Frame(10));
							} else {
								LOGGER.debug("Bonus " + secondChar);
								game.addFrame(new Frame(Integer.parseInt(Character.toString(secondChar))));
							}
						}
					} else if (StringUtils.isNumeric(frameString)) {
						Frame frame = new Frame(Integer.parseInt(frameString.substring(0, 1)));
						if (frameString.length() > 1) {
							frame.setSecondAttempt(new Integer(frameString.substring(1)));
						}
						game.addFrame(frame);
						LOGGER.debug("Bonus frame " + frame);
					} else {
						throw new ParseException(
							"Game " + score + " has invalid bonus balls " + frameString,
							score.lastIndexOf("|")
						);
					}
				}
			} else {
				throw new ParseException(
					"Game " + score + " has too many frames",
					score.lastIndexOf('|')
				);
			}
		}
		if (! game.isValid()) {
			throw new ParseException("Game " + score + " is not a valid game", 0);
		} else if (! game.isComplete()) {
			throw new ParseException("Game " + score + " is incomplete", score.length());
		}
		return game;
	}
	
	public void scoreGames() {
		int index = 0;
		for (Game game : games) {
			++index;
			System.out.println("Game " + index + ": " + game + "\n\tScore: " + game.getCurrentScore());
		}
	}

	public static void main(String[] args) {
		Options options = new Options();

        Option fileOption = new Option("f", "file", true, "Score data file");
        fileOption.setRequired(false);

        Option scoresOption = new Option("s", "scores", true, "Scores");
        scoresOption.setRequired(false);
        
        OptionGroup optionGroup = new OptionGroup();
        optionGroup.addOption(fileOption);
        optionGroup.addOption(scoresOption);
        optionGroup.setRequired(true);
        
        options.addOptionGroup(optionGroup);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;
        BowlingApp app = null;
        try {
            cmd = parser.parse(options, args);
            String fileName = cmd.getOptionValue("file");
            String scoreString = cmd.getOptionValue("scores");
            if (StringUtils.isNotBlank(fileName)) {
            	try (InputStream inputStream = new FileInputStream(new File(fileName))) {
	            	List<String> lines = IOUtils.readLines(inputStream);
	            	app = new BowlingApp(lines);
            	}
            } else if (StringUtils.isNoneBlank(scoreString)) {
            	app = new BowlingApp(scoreString);
            } else {
            	// If the command line parser is set up properly this should NEVER happen
            	// but let's be thorough.
            	throw new IllegalArgumentException("Either file or score must be provided.");
            }
            app.scoreGames();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            formatter.printHelp(
            	"BowlingApp -f path-to-file.txt\n"
	            	+ "BowlingApp -s bowling-score\n"
	            	+ "\n"
	            	+ "bowling-score syntax:\n"
	            	+ "\tframe|frame|frame|frame|frame|frame|frame|frame|frame|frame||frame\n"
	            	+ "\twhere frame = ## or X or just # or blank for last (11th) frame\n"
	            	+ "\twhere # = number 1-9 or / for spare or - for 0",
            	options
            );

            System.exit(-1);
        }
	}

}

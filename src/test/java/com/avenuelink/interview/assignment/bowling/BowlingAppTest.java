package com.avenuelink.interview.assignment.bowling;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.List;

import org.junit.Test;

import com.avenuelink.interview.assignment.bowling.beans.Game;

public class BowlingAppTest {

	private static final String[] SCORE_STRINGS = {
		"X|X|X|X|X|X|X|X|X|X||XX",
		"9-|9-|9-|9-|9-|9-|9-|9-|9-|9-||",
		"5/|5/|5/|5/|5/|5/|5/|5/|5/|5/||5",
		"X|7/|9-|X|-8|8/|-6|X|X|X||81"
	};
	private static final int[] SCORES = {
		300,
		90,
		150,
		167
	};
	
	@Test
	public void testApp() throws ParseException {
		for (int index = 0; index < SCORE_STRINGS.length; ++index) {
			String scoreString = SCORE_STRINGS[index];
			int score = SCORES[index];
			
			BowlingApp app = new BowlingApp(scoreString);
			
			// Test we have a game array, it is size 1 and has a game
			List<Game> games = app.getGames();
			assertNotNull("Expected game list " + scoreString, games);
			assertEquals("Expected a single game " + scoreString, 1, games.size());
			Game game = games.get(0);
			assertNotNull("Expected game " + scoreString, game);
			
			// Make sure game is valid
			assertTrue("Expected valid game but isn't " + scoreString, game.isValid());
			
			// Make sure game is complete
			assertTrue("Expected complete game but isn't " + scoreString, game.isComplete());
			
			// Test the score
			assertEquals("Game score not valid " + scoreString, score, game.getCurrentScore());
		}
	}
	
}

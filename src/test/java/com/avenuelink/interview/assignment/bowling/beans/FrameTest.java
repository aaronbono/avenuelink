package com.avenuelink.interview.assignment.bowling.beans;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FrameTest {
	
	@Test
	public void testFrameValidity() {
		// Typical valid frames with a few invalid because it's easy
		for (int firstScore = 0; firstScore <= 10; ++firstScore) {
			Frame frame = new Frame(firstScore);
			assertTrue(
				"Frame is supposed to be valid " + firstScore,
				frame.isValid()
			);
			for (int secondScore = 0; secondScore <= 10; ++secondScore) {
				frame.setSecondAttempt(secondScore);
				
				assertEquals(
					"Frame validity incorrect " + firstScore + ", " + secondScore,
					new Boolean((firstScore + secondScore) <= 10),
					new Boolean(frame.isValid())
				);
			}
		}
		
		{
			Frame frame = new Frame(20);
			assertFalse("Frame validity incorrect " + frame, frame.isValid());
		}

		{
			Frame frame = new Frame(20);
			frame.setSecondAttempt(-10);
			assertFalse("Frame validity incorrect " + frame, frame.isValid());
		}

		{
			Frame frame = new Frame(-1);
			frame.setSecondAttempt(11);
			assertFalse("Frame validity incorrect " + frame, frame.isValid());
		}

		{
			Frame frame = new Frame(9);
			assertTrue("Frame validity incorrect " + frame, frame.isValid());
		}

		{
			Frame frame = new Frame(10);
			assertTrue("Frame validity incorrect " + frame, frame.isValid());
		}
	}

	@Test
	public void testIsStrike() {
		
		// TODO I could spend days writing up different scenarios but I think
		//      the above test demonstrates how I approach testing.
		
	}

	@Test
	public void testIsSpare() {
		
		// TODO
		
	}

	@Test
	public void testCurrentScore() {
		
		// TODO
		
	}

	@Test
	public void testIsComplete() {
		
		// TODO
		
	}

}

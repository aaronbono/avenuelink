package com.avenuelink.interview.assignment.bowling.beans;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestGame {
	
	private static final int[][][] SCORE_TESTS = {
		{{1,4},{4,5},{6,4},{5,5},{10,0},{0,1},{7,3},{6,4},{10,0},{2,8},{6}},
		{{10,0},{7,3},{9,0},{10,0},{0,8},{8,2},{0,6},{10,0},{10,0},{10,0},{8,1}}
	};
	private static final int[] SCORE_TOTALS = {
		133,
		167
	};
	
	@Test
	public void testCurrentScore() {
		for (int index = 0; index < SCORE_TESTS.length; ++index) {
			int[][] gameScores = SCORE_TESTS[index];
			int total = SCORE_TOTALS[index];
			Game game = new Game();
			for (int[] frameScores : gameScores) {
				Frame frame = new Frame(frameScores[0]);
				if (frameScores.length > 1) {
					frame.setSecondAttempt(frameScores[1]);
				}
				game.addFrame(frame);
			}
			assertEquals("Scores not right for game " + gameScores, total, game.getCurrentScore());
		}
	}

	@Test
	public void testValid() {
		
		// TODO
		
	}

	@Test
	public void testComplete() {
		
		// TODO
		
	}

}
